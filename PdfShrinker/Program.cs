﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;

namespace PdfShrinker
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string fileToMerge = "eff0b34c-e81c-4e99-b438-dea1a045d921.pdf";

                //List<byte[]> pdfList = new List<byte[]>() { LoadPdf(fileToMerge), LoadPdf(fileToMerge) };
                List<byte[]> pdfList = new List<byte[]>() { LoadPdf(fileToMerge) };

                MergePdf(pdfList, out byte[] mergedPdf);

                string outputFilepath = "mergedPdf.pdf"; 
                File.WriteAllBytes(outputFilepath, mergedPdf);

                MergePdfUsingPdfSmartCopy(pdfList, out byte[] mergedPdfSmartCopy, false);

                string outputFilepathSmartCopy = "mergedPdfSmartCopy.pdf";
                File.WriteAllBytes(outputFilepathSmartCopy, mergedPdfSmartCopy);

                long sizeDifference = mergedPdfSmartCopy.Length - mergedPdf.Length;

                Console.WriteLine($"Tamanho utilizando PdfSmartCopy: {mergedPdfSmartCopy.Length} b");
                Console.WriteLine($"Tamanho utilizando novo merge: {mergedPdf.Length} b");
                Console.WriteLine($"Diferença: {sizeDifference} b");

                pdfList = new List<byte[]>() { mergedPdfSmartCopy };
                MergePdf(pdfList, out mergedPdf);
                string outputFilepathCombinedMethods = "mergedPdfCombinedMethods.pdf";
                File.WriteAllBytes(outputFilepathCombinedMethods, mergedPdf);

                Console.WriteLine($"Tamanho final: {mergedPdf.Length} b");
                File.WriteAllBytes(outputFilepathCombinedMethods, mergedPdf);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Erro inesperado: {ex.Message}");
            }

            Console.ReadKey();
        }

        static byte[] LoadPdf(string path)
        {
            return File.ReadAllBytes(path);
        }

        static void MergePdf(List<byte[]> pdfList, out byte[] mergedPdf)
        {
            mergedPdf = null;

            MemoryStream memoryStream = new MemoryStream();

            Document document = new Document(GetPdfPageSize(pdfList[0]));
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            document.Open();

            PdfContentByte contentByte = writer.DirectContent;

            foreach (byte[] pdf in pdfList)
            {
                PdfReader reader = new PdfReader(pdf);

                for (int i = 0; i < reader.NumberOfPages; i++)
                {
                    document.NewPage();

                    int pageNumber = i + 1;
                    PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);

                    contentByte.AddTemplate(page, 0, 0);
                }
            }

            document.Close();

            mergedPdf = memoryStream.ToArray();

            memoryStream.Close();
            writer.Close();
        }

        /// <summary>
        /// Reaproveita o lixo "metadata" gerado por outros PDF's
        /// </summary>
        /// <param name="pdfList"></param>
        /// <returns></returns>
        public static void MergePdfUsingPdfSmartCopy(List<byte[]> pdfList, out byte[] mergedPdf, bool compressFile = true)
        {
            mergedPdf = null;

            using (MemoryStream stream = new MemoryStream())
            {
                using (Document document = new Document())
                {
                    using (PdfSmartCopy pdfCopy = new PdfSmartCopy(document, stream))
                    {
                        if (compressFile)
                            pdfCopy.SetFullCompression();
                        
                        document.Open();

                        foreach (byte[] pdf in pdfList)
                        {
                            using (PdfReader reader = new PdfReader(pdf))
                            {
                                pdfCopy.AddDocument(reader);
                            }
                        }

                        document.Close();
                    }
                }

                mergedPdf = stream.ToArray();
            }
        }

        static Rectangle GetPdfPageSize(byte[] pdf)
        {
            PdfReader pageSizeReader = new PdfReader(pdf);
            Rectangle size = pageSizeReader.GetPageSizeWithRotation(1);

            pageSizeReader.Close();

            return size;
        }
    }
}
